# Indice
- [Indice](#indice)
- [Introduccion](#introduccion)
- [Ejecucion](#ejecucion)
- [Funcionamiento](#funcionamiento)
  * [C](#c)
  * [Assembler](#assembler)
- [Resultados](#resultados)

# Introduccion

En este repositorio de GIT se puede encontrar el trabajo practico 2 de la pateria Organizacion del Computador 2, de la carrera de sistemas de la UNGS.

Este trabajo practico consta de 2 archivos: un archivo assembler IA-32 (.s) y un archivo C.

Este sistema busca permitir que el usuario combine dos imagenes (en formato .rgb) utilizando una imagen mascara, es decir, que seleccione que pixeles de cada imagen debe devolver. 

**Esta funcionalidad se implementa en 2 formas distintas: una funcion directa en C, y una funcion en asm, utilizando instrucciones SIMD, por lo cual devuelve 2 archivos .rgb como resultado.**

# Ejecucion
Para ejecutar el programa, dirijase al directorio **/oc2-tp2**, y en la terminal ejecute el siguiente comando:

```bash
sh ejecutar.sh imagen1.rgb imagen2.rgb mascara.rgb ancho alto
```
`imagen1.rgb imagen2.rgb mascara.rgb ancho alto` seran los parametros pasados por el usuario

Dentro de las carpetas presentes en el repositorio se encuentran imagenes ya convertidas a .rgb y listas para ejecutar el programa. Para ejecutar el programa puede copiar los archivos en /oc2-tp2, o utilizar sus propias imagenes rgb.

El archivo .sh se encargará de compilar el codigo asm y el codigo c, linkearlos y ejecutar el programa.

# Funcionamiento 

En este apartado se explicara como fue resuelto el problema y que metodos fueron utilizados, y se mostraran capturas del funcionamiento del programa.

## C
Por el lado de C, se busca que el usuario pueda ingresar por linea de comandos los parametros a utilizar (imagen 1, imagen 2, mascara, ancho, alto), y ejecute asi la funcion de enmascarar escrita en asm, y la escrita en este mismo archivo c.

Para poder llevar a cabo la ejecucion completa del programa, el archivo C declara a la funcion externa `enmascarar_asm`, e implementa una funcion propia llamada `enmascarar_c`, realizando un llamado a la mismas pasandole los valores que el usuario ingreso por linea de comandos.

Para que el archivo C encuentre la funcion resolvente proveniente del archivo Assembler, estos son linkeados a la hora de compilar el archivo C.

A continuacion se explicaran los distintos fragmentos de codigo, mostrando que hace cada una de las funciones del archivo c.

---

### Declaracion de buffers

```C
char *bufferA;
char *bufferAsm;
char *bufferB;
char *bufferMask;
```
Se declaran los buffers como variables globales, ya que seran utilizados a lo largo de todo el programa.

---

### Cargar buffers

```C
void cargar_buffers(FILE *imgA, FILE *imgAsm, FILE *imgB, FILE *mask, int imgSize){
	bufferA=(unsigned char *) malloc(imgSize);
	bufferAsm=(unsigned char *) malloc(imgSize);
	bufferB=(unsigned char *) malloc(imgSize);
	bufferMask=(unsigned char *) malloc(imgSize);

    fread(bufferA, 1, imgSize, imgA);
	fread(bufferAsm, 1, imgSize, imgAsm);
	fread(bufferB, 1, imgSize, imgB);
	fread(bufferMask, 1, imgSize, mask);
}
```
Esta funcion se encarga de reservar el espacio en memoria necesario para cada buffer (para poder almacenar todas las imagenes), y leer los archivos FILE (abiertos en la funcion main) y escribirlos dentros de los buffers.

BufferA y bufferASM contienen una copia de la misma imagen, utilizadas una en cada funcion (una en asm, una en c).

---

### Calcular tamaño de imagen

```C
int calcularTamanio(int ancho, int alto){
	int resultado;
	resultado=ancho*alto*3;
	return resultado;	
}
```
Esta funcion recibe por parametro el ancho y alto de las imagenes, y calcula cual seria el tamaño del archivo en bytes, teniendo en cuenta que los archivos rgb contienen 3 bytes por pixel.

Este dato sera utilizado a la hora de reservar el espacio para los buffers.

---

### Funcion para enmascarar (C)

```C
void enmascarar_c(unsigned char *bufferA,unsigned char *bufferB,unsigned char *bufferMask,int imgSize){
	FILE *imgOut;	
	for(int i=0;i<imgSize;i=i+3){
		if(bufferMask[i]==255 && bufferMask[i+1]==255 && bufferMask[i+2]==255){
			bufferA[i]=bufferB[i];
			bufferA[i+1]=bufferB[i+1];
			bufferA[i+2]=bufferB[i+2];	
		}
	}
	imgOut=fopen("salida_c.rgb","wb");
	fwrite(bufferA, 1, imgSize, imgOut);
	fclose(imgOut);
}
```
Esta es la funcion que permite enmascarar las imagenes otorgadas por el usuario, implementada en lenguaje C.

Esta funcion recibe por parametro los buffers correspondientes a las dos imagenes y la mascara, y el tamaño de las imagenes en bytes.

Utiliza un ciclo for para recorrer los buffers de a 3 bytes (ya que 3 bytes conforma un pixel), y evalua si los pixeles de la mascara son completamente blancos (los bytes son 255,255,255):

**En el caso de que lo sean, reemplaza los bytes que se encuentran en esa misma posicion del buffer A, por los que se encuentran en esa posicion en el buffer B. En caso contrario, no hace nada.**

Al finalizar el ciclo, escribe la informacion guardada en el buffer A (ahora con los pixeles correspondientes reemplazados) en un nuevo archivo rgb, llamado `salida_c.rgb`

---

### Main

```C
int main (int argc, char *argv[]){
	FILE *imgA;
	FILE *imgAsm;
	FILE *imgB;
	FILE *mask;
	int imgSize;
	int imgAncho;
	int imgAlto;	

	imgA=fopen(argv[1],"rb");
	imgAsm=fopen(argv[1],"rb");
	imgB=fopen(argv[2],"rb");
	mask=fopen(argv[3],"rb");
	
	imgAncho=atoi(argv[4]);
	imgAlto=atoi(argv[5]);
	
	imgSize=calcularTamanio(imgAncho,imgAlto);

	cargar_buffers(imgA, imgAsm, imgB, mask, imgSize);	
	enmascarar_c(bufferA, bufferB, bufferMask, imgSize);
	enmascarar_asm(bufferAsm, bufferB, bufferMask, imgSize);

	FILE *asmOut;
	asmOut=fopen("salida_asm.rgb","wb");
	fwrite(bufferAsm, 1, imgSize, asmOut);
	fclose(asmOut);

	fclose(imgA);
	fclose(imgAsm);
	fclose(imgB);
	fclose(mask);

	printf("Archivos creados!\n");

	return 0;
}
```

Esta es la funcion principal del programa. Abre los archivos .rgb que el archivo selecciona por parametro, y obtiene el tamaño de las imagenes mediante la funcion `calcularTamanio`.

Utiliza estos datos para cargar los buffers mediante la funcion `cargar_buffers`, y llama a las funciones `enmascarar_c` y `enmascarar_asm` pasandoles como parametro los buffers y el tamañode las imagenes.

**Estas dos funciones se encargaran de enmascarar las imagenes y cargarlas en los buffers correspondientes**

La funcion `enmascarar_c` guarda el resultado en un archivo nuevo, mientras que el resultado de `enmascarar_asm` es escrito en un archivo desde el main (`fwrite(bufferAsm, 1, imgSize, asmOut);`)

Por ultimo, cierra los archivos abiertos e imprime por pantalla un mensaje, indicando que los archivos fueron creados correctamente.

---

## Assembler

Desde el lado del assembler...

A continuacion seran explicados los distintos fragmentos de codigo que contiene el archivo assembler.

----

### Inicializacion de variables

```s
section .data
tiraBlanco db 255,255,255,255,255,255,255,255
```
Se inicializa un arreglo de 8 bytes seteados en 255, el cual sera utilizado mas tarde para invertir la mascara. 

----

### Preparar la pila

```nasm
section .text
enmascarar_asm:
    push ebp
    mov ebp, esp
    
    mov ebx, [ebp+20] 
    mov [imgSize]
    mov ecx, 0
```
Se prepara la pila para recibir los parametros. Guarda el tamaño de la imagen (`[ebp+20]`) en el registro `ebx`, el cual ira decrementando en el recorrido del programa para utilizarse como contador.

Setea el registro `ecx` en 0, el cual sera utilizado luego para recorrer los buffers.

----

### Recorrido normal

```nasm
cicloNormal:
    cmp ebx, 8
    jnae ultimosBytes
```
Al llegar al primer ciclo, se realiza un check para saber si la cantidad de bytes que falta por recorrer es mayor o igual a 8.

Si se cumple esta condicion, sigue con la ejecucion normal, en caso contrario salta a la etiqueta ultimosBytes

**Esto permite que no se genere un segmentation fault por intentar ingresar a posiciones fuera del buffer**

```   
    mov edx, [ebp+8]
    movq mm0, qword[edx+ecx] 
    mov eax, [ebp+12]
    movq mm1, qword[eax+ecx]  
    mov eax, [ebp+16]
    movq mm2, qword[eax+ecx]  
    movq mm3, qword[tiraBlanco]  
```
`[ebp+8]` = imagen 1, `[ebp+12]` = imagen 2, `[ebp+16]` =  mascara

Se cargan los punteros que indican la posicion de los buffers en los registros MMX, y el array `tiraBlanco` anteriormente inicializado, para poder invertir la mascara.

El buffer correspondiente a la primer imagen es el unico que no utiliza el registro eax, ya que esta misma direccion debe ser utilizada mas tarde dentro del ciclo para guardar los datos modificados.

El valor del registro `ecx` va a ir aumentando de 8 en 8 (cantidad de bytes que admite cada registro MMX), para poder recorrer los buffers al completo.

```nasm
    pand mm1,mm2
    pxor mm2,mm3
    pand mm0,mm2
    por mm0,mm1
    
    movq qword[edx+ecx], mm0 
    
    add ecx, 8 
    sub ebx, 8

    jmp cicloMayor8;
```
Utiliza operadores logicos para trabajar sobre los datos empaquetados correspondientes a los bytes tomados de los buffers.

Borra de la imagen 2 los pixeles 
correspondientes a la parte negra de la mascara, y de la imagen 1 los correspondientes a la parte blanca, suma los restantes y los guarda en la posicion de memoria del buffer 1.

Suma 8 al `ecx`, para recorrer los proximos 8 bytes de los buffers en la proxima iteracion, y resta 8 al `ebx` para indicar que ya se leyeron 8 bytes del total.

Vuelve a saltar al inicio del ciclo, para seguir la ejecucion con los proximos pixeles.

----

### Ultimos bytes

```nasm
ultimosBytes:
    cmp ebx, 0
    je fin
    add ebx, ecx
    
    mov edx, [ebp+8]
    movq mm0, qword[edx+ebx]
    mov eax, [ebp+12]
    movq mm1, qword[eax+ebx]
    mov eax, [ebp+16]
    movq mm2, qword[eax+ebx]
    
    movq mm3, qword[tiraBlanco]  
    
    pand mm1,mm2
    pxor mm2,mm3
    pand mm0,mm2
    por mm0,mm1

    movq qword[edx+ebx], mm0
```
El programa salta a esta etiqueta si detecta que quedan menos de 8 bytes para recorrer en los buffers.

Este fragmento de codigo realiza lo mismo que el ciclo de ejecucion normal, pero con la diferencia que en vez de sumar 8 a la posicion del puntero de cada buffer, suma la cantidad de bytes restantes.

Esto permite moverse solo las posiciones necesarias para llegar hasta el ultimo byte de los buffers, sin pasarse y asi evitar segmentation faults.

----

# Resultados

Luego de probar el sistema con imagenes de distintos tamaños, estos fueron los resultados (se muestran las imagenes 1 y 2, la mascara, y el resultado).

## 640 x 360
![360p](./ImagenesReadme/salida360p.jpg)

## 1280 x 720
![720p](./ImagenesReadme/salida720p.jpg)

## 1920 x 1080
![1080p](./ImagenesReadme/salida1080p.jpg)

## 3840 x 2160
![4k](./ImagenesReadme/salida4k.jpg)
