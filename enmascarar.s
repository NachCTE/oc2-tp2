global enmascarar_asm

section .data
tiraBlanco db 255,255,255,255,255,255,255,255

section .bss
imgSize resq 1

section .text
enmascarar_asm:
    push ebp
    mov ebp, esp
    
    mov ebx, [ebp+20]       ;Guardo el tamanio de la imagen en ebx
    mov [imgSize], ebx      ;Guardo el dato en imgSize, ebx sera utilizado como contador
    
    mov ecx, 0
    
cicloNormal:
    cmp ebx, 8  ;si el contador de bytes restantes es < 8, salto
    jnae ultimosBytes
    
    mov edx, [ebp+8]
    movq mm0, qword[edx+ecx] ;imagen1 
    mov eax, [ebp+12]
    movq mm1, qword[eax+ecx]  ;imagen2
    mov eax, [ebp+16]
    movq mm2, qword[eax+ecx]  ;mascara
    
    movq mm3, qword[tiraBlanco]  
    
    pand mm1,mm2; borro de la imagen 2 los canales de pixeles donde la mascara es negra
    pxor mm2,mm3; creo una mascara inversa
    pand mm0,mm2; borro de la imagen 1 los canales de pixeles donde la imagen es blanca
    por mm0,mm1; sumamos las dos imagenes alteradas y guardo el resultado en la imagen 1
    
    movq qword[edx+ecx], mm0    ;Guardo la imagen 1 ya modificada
    
    add ecx, 8 
    sub ebx, 8

    jmp cicloNormal;
    
    
ultimosBytes:
    cmp ebx, 0
    je fin
    add ebx, ecx
    
    mov edx, [ebp+8]
    movq mm0, qword[edx+ebx] ;imagen1 
    mov eax, [ebp+12]
    movq mm1, qword[eax+ebx]  ;imagen2
    mov eax, [ebp+16]
    movq mm2, qword[eax+ebx]  ;mascara
    
    movq mm3, qword[tiraBlanco]  
    
    pand mm1,mm2; borro de la imagen 2 los canales de pixeles donde la mascara es negra
    pxor mm2,mm3; creo una mascara inversa
    pand mm0,mm2; borro de la imagen 1 los canales de pixeles donde la imagen es blanca
    por mm0,mm1; sumamos las dos imagenes alteradas y guardo el resultado en la imagen 1
    
    movq qword[edx+ebx], mm0    ;Guardo la imagen 1 ya modificada


fin:
    mov ebp, esp
    pop ebp
    ret
    
    
