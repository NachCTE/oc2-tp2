#include <stdio.h>
#include <stdlib.h>

extern void enmascarar_asm(unsigned char *bufferAsm,unsigned char *bufferB,unsigned char *bufferMask,int imgSize);

// parametros: imagen 1, imagen 2, mascara, ancho, alto.

char *bufferA;
char *bufferAsm;
char *bufferB;
char *bufferMask;

void cargar_buffers(FILE *imgA, FILE *imgAsm, FILE *imgB, FILE *mask, int imgSize){
	bufferA=(unsigned char *) malloc(imgSize);
	bufferAsm=(unsigned char *) malloc(imgSize);
	bufferB=(unsigned char *) malloc(imgSize);
	bufferMask=(unsigned char *) malloc(imgSize);

	//Guardo los RGB en los buffers.
    	fread(bufferA, 1, imgSize, imgA);
	fread(bufferAsm, 1, imgSize, imgAsm);
	fread(bufferB, 1, imgSize, imgB);
	fread(bufferMask, 1, imgSize, mask);
}

void enmascarar_c(unsigned char *bufferA,unsigned char *bufferB,unsigned char *bufferMask,int imgSize){
	FILE *imgOut;	
	for(int i=0;i<imgSize;i=i+3){
		if(bufferMask[i]==255 && bufferMask[i+1]==255 && bufferMask[i+2]==255){
			bufferA[i]=bufferB[i];
			bufferA[i+1]=bufferB[i+1];
			bufferA[i+2]=bufferB[i+2];	
		}
	}
	imgOut=fopen("salida_c.rgb","wb");
	fwrite(bufferA, 1, imgSize, imgOut);
	fclose(imgOut);
}

int calcularTamanio(int ancho, int alto){
	int resultado;
	resultado=ancho*alto*3;
	return resultado;	
}

int main (int argc, char *argv[]){
	FILE *imgA;
	FILE *imgAsm;
	FILE *imgB;
	FILE *mask;
	int imgSize;
	int imgAncho;
	int imgAlto;	

	imgA=fopen(argv[1],"rb");
	imgAsm=fopen(argv[1],"rb");
	imgB=fopen(argv[2],"rb");
	mask=fopen(argv[3],"rb");
	
	imgAncho=atoi(argv[4]);
	imgAlto=atoi(argv[5]);
	
	imgSize=calcularTamanio(imgAncho,imgAlto);

	cargar_buffers(imgA, imgAsm, imgB, mask, imgSize);	
	
	//Llamo a la funcion enmascarar_c.
	enmascarar_c(bufferA, bufferB, bufferMask, imgSize);

	//llamo a la funcion enmascarar_asm.
	enmascarar_asm(bufferAsm, bufferB, bufferMask, imgSize);

	FILE *asmOut;
	asmOut=fopen("salida_asm.rgb","wb");
	fwrite(bufferAsm, 1, imgSize, asmOut);
	fclose(asmOut);

	//Cierro los archivos abiertos.	
	fclose(imgA);
	fclose(imgAsm);
	fclose(imgB);
	fclose(mask);
	
	//Notifico que termino la ejecucion.
	printf("\nArchivos creados!\n");

	return 0;
}




